<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="node_modules/semantic-ui/dist/semantic.min.css">

    
    <title>Document</title>
</head>
<body>

<?php
 
//Open our CSV file using the fopen function.
$fh = fopen("cs_figures.csv", "r");


//  Pour new ligne ajouter dans cs_figures.csv, afficher dans une card semantic, les infos de la personne
// var_dump($fh);

$ligne = fgets($fh); //lit la 1ere ligne du fichier
// echo $ligne;

// Setup a PHP array to hold our CSV rows.
$csvData = array();
 
//Loop through the rows in our CSV file and add them to
//the PHP array that we created above.
while (($row = fgetcsv($fh, 0, ",")) !== FALSE) {
   $csvData[] = $row;
 }


 $ligne = false;

    $parse = json_encode($csvData);
    $fileLines=file("cs_figures.csv");

    ?>

    <div class="ui three column grid">

    <?php
    for($i = 0; $i < count($fileLines); $i++){

        if($ligne = false){
            
         return $ligne;
         }
        else{
        echo "<div class=\"column\">";
        echo "<div class=\"ui card\">";
        echo "<div class=\"image\">";
        echo "<img src=".json_encode($csvData[$i][5]). ">";
        echo "<div class=\"content\">";
        echo "<div class=\"header\">".json_encode($csvData[$i][0])."</div>";
        echo "</div>";
        echo "<div class=\"meta\">";
        echo "<div class=\"description\">".json_encode($csvData[$i][2])."</div>";
        echo "</div>";
        echo "<div class=\"description\">".json_encode($csvData[$i][3])."</div>";
        echo "<div class=\"extra content\"> <span class=\"right floated\">Born in ".json_encode($csvData[$i][1])."</span></div>";
        echo "</div>";
        echo "</div>";
        }    
    }
    ?>
</div>
<?php

?>


<!-- <div class="ui three cards">

<section class="ui raised card">
    <a href="pages/Charle.php">
    <div class="ui card">
        <div class= "image">
            <img src= "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/Charles_Babbage_-_1860.jpg/220px-Charles_Babbage_-_1860.jpg"> 
        </div>
        <div class="content">
           <div class="header">
                <?php
                    echo json_encode($csvData[1][0]);
                ?>
           </div>
           <div class="meta">
                <div class="description">
                    <?php
                        echo json_encode($csvData[1][2]);
                    ?>
                </div>
           </div>
          
           <div class="description">
           <?php
                    echo json_encode($csvData[1][3]);
                ?>
           </div>
           <div class="extra content">
                Born in 
                <?php
                    echo json_encode($csvData[1][1]);
                ?>
           </div>

        </div>
    </div>
    </a>
</section>

</div> -->

<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
<script src="node_modules/semantic-ui/dist/semantic.js"></script>
<script src="node_modules/semantic-ui/dist/semantic.min.js"></script>
<script> src="Exercices/php-composing-master/script.js"</script>
</body>
</html>